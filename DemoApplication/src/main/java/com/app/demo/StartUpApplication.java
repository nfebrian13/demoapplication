package com.app.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.app.demo.scheduler.Scheduler;
import com.app.demo.service.impl.ProductServiceImpl;

@Component
public class StartUpApplication implements ApplicationRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(StartUpApplication.class);

	@Autowired
	private ProductServiceImpl productService;

	@Autowired
	private Scheduler scheduler;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub

		try {
			productService.productRepo();
			scheduler.fixedRateSch();
//			scheduler.cronJobSch();
//			scheduler.fixedDelaySch();

			LOGGER.info("StartUpApplication wqwqwq");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
