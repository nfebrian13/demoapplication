package com.app.demo.scheduler;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.SchedulingException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {

	private static final Logger LOGGER = LoggerFactory.getLogger(Scheduler.class);

	/*
	 * Cron Scheduler
	 * 
	 */

	@Scheduled(cron = "0 * 9 * * ?")
	public void cronJobSch() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date now = new Date();
			String strDate = sdf.format(now);
			LOGGER.info("Java cron job expression:: " + strDate);

		} catch (SchedulingException e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	}

	/*
	 * Fixed Rate Scheduler
	 * 
	 */

	@Scheduled(fixedRate = 1000)
	public void fixedRateSch() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date now = new Date();
			String strDate = sdf.format(now);

			LOGGER.info("Fixed Rate scheduler::" + strDate);
		} catch (SchedulingException e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	}

	/*
	 * Fixed Delay Scheduler
	 * 
	 */
	@Scheduled(fixedDelay = 1000, initialDelay = 3000)
	public void fixedDelaySch() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date now = new Date();
			String strDate = sdf.format(now);
			LOGGER.info("Fixed Delay scheduler::" + strDate);
		} catch (SchedulingException e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
	}
	
}