package com.app.demo.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.demo.StartUpApplication;
import com.app.demo.model.Product;
import com.app.demo.repository.ProductRepository;
import com.app.demo.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	private static final Logger LOGGER = LoggerFactory.getLogger(StartUpApplication.class);

	@Autowired
	private ProductRepository productRepository;

	@Override
	public Map<String, Product> productRepo() {
		LOGGER.info("productService....");
		return productRepository.productRepo();
	}

	@Override
	public Map<String, Product> productRepoList() {
		return productRepository.productRepoList();
	}

	@Override
	public boolean createProduct(Product product) {
		return productRepository.createProduct(product);
	}

	@Override
	public boolean updateProduct(String key, Product product) {
		return productRepository.updateProduct(key, product);
	}

	@Override
	public boolean deleteProduct(String key) {
		return productRepository.deleteProduct(key);
	}

}
