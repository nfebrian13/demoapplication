package com.app.demo.service;

import java.util.Map;

import com.app.demo.model.Product;

public interface ProductService {

	public Map<String, Product> productRepo();

	public Map<String, Product> productRepoList();

	public boolean createProduct(Product product);

	public boolean updateProduct(String key, Product product);

	public boolean deleteProduct(String key);
}
