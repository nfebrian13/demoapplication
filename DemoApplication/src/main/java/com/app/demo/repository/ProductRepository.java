package com.app.demo.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.app.demo.model.Product;

@Repository
public class ProductRepository {

	Map<String, Product> productRepo = new HashMap<>();

	public Map<String, Product> productRepo() {

		Product honey = new Product();
		honey.setId("1");
		honey.setName("Honey");
		productRepo.put(honey.getId(), honey);

		Product almond = new Product();
		almond.setId("2");
		almond.setName("Almond");
		productRepo.put(almond.getId(), almond);
		return productRepo;
	}
	
	public Map<String, Product> productRepoList() {
		System.out.println("productRepo size " + productRepo.size());
		return productRepo;
	}

	public boolean createProduct(Product product) {
		try {
			Product prodObj = new Product();
			prodObj.setId(product.getId());
			prodObj.setName(product.getName());
			productRepo.put(prodObj.getId(), prodObj);
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("printStackTrace " + e.getMessage());
		}

		return false;
	}

	public boolean updateProduct(String key, Product product) {
		try {
			productRepo.remove(key);
			System.out.println("product size rmv: " + productRepo.size());
			
			Product prodObj = new Product();
			prodObj.setId(key);
			prodObj.setName(product.getName());
			productRepo.put(key, prodObj);
			System.out.println("product size put: " + productRepo.size());
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("printStackTrace " + e.getMessage());
		}

		return false;
	}

	public boolean deleteProduct(String key) {
		try {
			productRepo.remove(key);
			System.out.println("product size: " + productRepo.size());

			return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("printStackTrace " + e.getMessage());
		}
		return false;
	}

}
