package com.app.demo.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/*
 * Filter using for: 
 * Before sending the request to the controller
 * Before sending a response to the client.
 * 
 * */

@Component
public class SimpleFilter implements Filter {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleFilter.class);

	@Override
	public void destroy() {
		LOGGER.info("destroy calling....");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		LOGGER.info("doFilter");
		LOGGER.info("Remote Host:" + request.getRemoteHost());
		LOGGER.info("Remote Address:" + request.getRemoteAddr());
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig filterconfig) throws ServletException {
		LOGGER.info("init calling....");
	}

}